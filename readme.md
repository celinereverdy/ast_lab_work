# AST Lab Work

## This project is for:
Creating a static website with NodeJS (details in [TP.md](https://gitlab.com/celinereverdy/ast_lab_work/blob/master/TP.md)).

Our website takes the form of a blog with an index page referencing a list of articles, and a page for each detailed article.
For simplicty, we listed only one article.
The page with the details of an article can be reached by clicking on the article in the index page.
The top right navigation button brings you back to the index page.

## Set up dependencies:
* `npm install`

## Run:
* `npm start`
* browse to `127.0.0.1:8888` or `localhost:8888`

## Test:
* `npm test`

## Contributors:
* Céline Reverdy
* Rina Attieh
