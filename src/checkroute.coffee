fs = require 'fs'

module.exports =
   # function to get HTTP code 200 if file exists and 404 if it doesn't
   getHttpCode: (filepath) ->
      if fs.existsSync(filepath)
         return 200
      else
         return 404
