checkroute = require "./checkroute.coffee"
url = require 'url'
fs = require 'fs'
pug = require 'pug'

renderResource = (filename, type, res) ->
   if type == "html" # pug rendering
      console.log "rendering pug resource #{filename}"
      pug.renderFile "views/#{filename}", pretty: true, (err, html) ->
         if err
            code = checkroute.getHttpCode "views/#{filename}"
            res.writeHead code,
               'Content-Type': 'text/plain'
            res.end 'File not found\n'
         else
            code = checkroute.getHttpCode "views/#{filename}"
            res.writeHead code,
               'Content-Type': "text/#{type}; charset=UTF-8"
            res.write html
            res.end()

   else # i.e. css or images
      console.log "rendering resource #{filename} of type #{type}"
      fs.readFile "public/#{type}/#{filename}", (err, file) ->
         if err
            code = checkroute.getHttpCode "public/#{type}/#{filename}"
            res.writeHead code,
               'Content-Type': 'text/plain'
            res.end 'File not found\n'
         else
            code = checkroute.getHttpCode "public/#{type}/#{filename}"
            res.writeHead code,
               'Content-Type': "text/#{type}"
            res.write file
            res.end()

module.exports =
   logic: (req, res) ->
      url = url.parse req.url
      [ _, directory, filetype, filename ] = url.pathname.split "/"
      directory = "/" if directory == ""

      switch directory
         when "/"
            renderResource "index.pug", "html", res
         when "usa"
            renderResource "usa.pug", "html", res
         when "public"
            renderResource filename, filetype, res
         else
            code = checkroute.getHttpCode(directory)
            res.writeHead code,
               'Content-Type': 'text/plain'
            res.end 'Route not found\n'

    port: "8888"
    address: "127.0.0.1"
