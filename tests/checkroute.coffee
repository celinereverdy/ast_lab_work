should = require 'should'
checkroute = require '../src/checkroute.coffee'

# simple unit testing for routing
describe 'routing', () ->

   it 'index.pug should return 200', (done) ->
      code = checkroute.getHttpCode "views/index.pug"
      code.should.equal 200
      done()

   it 'usa.pug should return 200', (done) ->
      code = checkroute.getHttpCode "views/usa.pug"
      code.should.equal 200
      done()

   it 'non-existing file should return 404', (done) ->
      code = checkroute.getHttpCode "file_that_doesnt_exist"
      code.should.equal 404
      done()
